import React from "react"
import { css } from "@emotion/core"
import { useStaticQuery, graphql } from "gatsby"
import AniLink from "gatsby-plugin-transition-link/AniLink"

import { rhythm } from "../utils/typography"
export default ({ children }) => {
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
      }
    `
  )
  return (
    <div
      css={css`
        margin: 0 auto;
        max-width: 700px;
        padding: ${rhythm(2)};
        padding-top: ${rhythm(1.5)};
      `}
    >
      <AniLink paintDrip to="/" hex="#A987A8">
        <h3
          css={css`
            margin-bottom: ${rhythm(2)};
            display: inline-block;
            font-style: normal;
          `}
        >
          {data.site.siteMetadata.title}
        </h3>
      </AniLink>
      <AniLink
        fade
        to="/about/"
        css={css`
          float: right;
          text-decoration: none;
        `}
      >
        <small>about</small>
      </AniLink>

      <AniLink
        to="/my-files"
        swipe
        direction="right"
        css={css`
          float: right;
          margin-right: 10px;
          text-decoration: none;
        `}
      >
        <small>files</small>
      </AniLink>

      <AniLink
        to="/sitemap.xml"
        swipe
        direction="right"
        css={css`
          float: right;
          margin-right: 10px;
          text-decoration: none;
        `}
      >
        <small>sitemap</small>
      </AniLink>
      {children}
    </div>
  )
}
